LOCATION=eastus
USERNAME=liam
PASSWORD="1@Barracuda13"
VMNAME=vm-test
RG=rg-test
VNetName=vnet-test
SNetName=snet-test
Size=Standard_DS2_v2
Windows=Y
Image=Canonical:0001-com-ubuntu-server-focal:20_04-lts:latest

az group create -l $LOCATION -n $RG

az network vnet create -g $RG -n $VNetName –address-prefix 20.0.0.0/16 –subnet-name MySubnet –subnet-prefix 20.0.0.0/24

if Windows 
az vm create \
  –name $VMNAME \
  –resource-group $RG \
  –image $Image \
  –size $Size \
  –location $LOCATION \
  –admin-username $USERNAME \
  –admin-password $PASSWORD \
  –vnet-name $VNetName –subnet MySubnet

az vm get-instance-view \
  –name $VMNAME \
  –resource-group $RG \
  –output table